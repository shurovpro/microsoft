﻿#Import-Module ActiveDirectory
Param (
    [Parameter(Mandatory=$True)]
    $givenname,
    [Parameter(Mandatory=$True)]
    $surname,
    [Parameter(Mandatory=$True)]    
    $template
)
$password = get-random -count 9 -input (33..57 + 65..90 + 97..122) | % -begin { $pass = $null } -process {$pass += [char]$_} -end {$pass}
$name = "$givenname $surname"
$samaccountname = "$($givenname[0])$surname"
$password_ss = ConvertTo-SecureString -String $password -AsPlainText -Force
$template_obj = Get-ADUser -Identity $template -Properties MemberOf,Office,City, Country,Company
$userprincipalname = ($samaccountname+"@" + (($template_obj.userPrincipalName -split "@")[1]))
$ou = $template_obj.DistinguishedName -replace '^cn=.+?(?<!\\),'

$params = @{
    "SamAccountName" =$samaccountname
    "UserPrincipalName" = $userprincipalname 
    "Instance"=$template_obj
    "Name"=$name
    "DisplayName"=$name
    "GivenName"=$givenname
    "SurName"=$surname
    "AccountPassword"=$password_ss
    "Enabled"=$true
    "ChangePasswordAtLogon"=$True
    "Path" = $ou
    "PasswordNeverExpires" = $false
    "CannotChangePassword" = $false
}
New-ADUser @params
$template_obj.memberof | add-adgroupmember -members $samaccountname
Write-Host "-------------New User created-------------------------"
Write-Host "Login: " $samaccountname
Write-Host "UPN: " $userprincipalname
Write-Host "Password: " $Password 
Write-Host "OU: " $ou 
Write-Host "------------------------------------------------------"